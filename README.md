 # xy2xrdml

 A Python script to convert powder X-ray diffraction pattern data files in
 form of data points to XRDML, which includes wavelength information.


 ## Usage

To convert all files in the current working directory with given extension, use:
```bash
python xy2xrdml.py -l <wavelength> -e <file extension>
```

To display help, use:
```bash
python xy2xrdml.py --help
```

The script (`xy2xrdml.py`) has to either be present in the current directory or
reside in a directory, added to PATH. Otherwise path to the script needs to be
specified (for example `/home/user/myscripts/xy2xrdml.py`).


## Details

The scripts treats input files as numbers in whitespace-separated columns.
Each line needs to have at least two columns, otherwise it is ignored. The first
column represents 2θ in degrees, the second intensity, and other columns are
ignored.

Passing the wavelength as command-line argument is mandatory. The wavelength
should be in Å. File extension is not case-sensitive.


## Other tips

### Getting Python

Python 3 interpreter is required to run this script. Python for Windows can be
obtained from [this official site](https://www.python.org/downloads/windows/).
Pick the latest stable release of Python 3. When installing Python, **tick
the option to add Python to PATH!** This will allow it to be used from any
working directory.

### Opening command prompt

When working on Windows, command prompt can be opened in the folder, currently
open in the explorer, by **right-clicking** in empty folder space while holding
**Shift**. The context menu, that appears, should offer the option to either
open Command Prompt or PowerShell in that folder.