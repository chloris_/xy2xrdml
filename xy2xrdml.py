#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 15 14:26:24 2018

Converts powder pattern data point files (2θ, intensity) to XRDML files with included
wavelength information.

@author: Chloris
@version: 1.1.0
@license: GPLv3
"""


class Xy2Xrdml:
    
    from pathlib import Path
    
    def __init__(self, file_name, wavelength):
        self.__file_name = file_name
        self.__wavelength = wavelength
        self.__2theta_start = 0.0
        self.__2theta_end = 0.0
        self.__intensities = []
    
    def __read_input_file(self):
        line_counter = 0
        ttheta = 0
        
        with open(self.__file_name, "r") as ifile:
            for line in ifile:
                line_counter += 1
                temporary_words = line.split(" ")
                words = []
                
                # Remove empty strings
                for s in temporary_words:
                    if s:
                        words.append(s)
                del temporary_words
                
                # Does line have enough words?
                if len(words) < 2:
                    print("Skipping bad line {0} in file '{1}'".format(
                            line_counter, self.__file_name))
                    continue
                
                # Extract 2theta and intensity
                try:
                    ttheta = float(words[0])
                    intensity = float(words[1])
                except ValueError:
                    print("Failed to convert values in line {0} of "
                          "file '{1}'".format(line_counter, self.__file_name))
                    continue
                
                # Store data
                if line_counter == 1:
                    self.__2theta_start = ttheta
                self.__intensities.append(intensity)
            self.__2theta_end = ttheta
    
    def __get_output_file_name(self):
        i = self.__file_name.rfind(".")
        ofile = self.__file_name[:i] + ".xrdml"
        return ofile
    
    def __write_output_file(self):
        from datetime import datetime

        output_file_name = self.__get_output_file_name()
        
#        if Path(output_file_name).is_file():
#            print("File '{0}' already exists and will not be written" \
#                  .format(output_file_name))
#            return
        
        is_first = True
        
        with open(output_file_name, "w", newline = "\r\n") as out_file:
            timestamp = datetime.today().strftime("%Y-%m-%dT%H:%M:%S")

            out_file.write(
"""<?xml version="1.0" encoding="UTF-8"?>
<xrdMeasurements xmlns="http://www.xrdml.com/XRDMeasurement/1.3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.xrdml.com/XRDMeasurement/1.3 http://www.xrdml.com/XRDMeasurement/1.3/XRDMeasurement.xsd" status="Completed">
	<xrdMeasurement measurementType="Scan" status="Completed">
		<usedWavelength intended="K-Alpha 1">
			<kAlpha1 unit="Angstrom">{wavelength}</kAlpha1>
			<kAlpha2 unit="Angstrom">0.000000</kAlpha2>
			<kBeta unit="Angstrom">0.000000</kBeta>
			<ratioKAlpha2KAlpha1>0.000000</ratioKAlpha2KAlpha1>
		</usedWavelength>
		<incidentBeamPath>
			<xRayTube>
				<anodeMaterial>Other</anodeMaterial>
			</xRayTube>
		</incidentBeamPath>
		<scan appendNumber="0" mode="Continuous" scanAxis="2Theta" status="Completed">
			<header>
				<startTimeStamp>{timestamp}</startTimeStamp>
				<author>
					<name>xy2xrdml.py</name>
				</author>
			</header>
			<dataPoints>
				<positions axis="2Theta" unit="deg">
					<startPosition>{start_position:.3f}</startPosition>
					<endPosition>{end_position:.3f}</endPosition>
				</positions>
				<commonCountingTime unit="seconds">1.0</commonCountingTime>
""" \
            .format(
                wavelength = self.__wavelength,
                start_position = self.__2theta_start,
                end_position = self.__2theta_end,
                timestamp = timestamp
            ))
            
            out_file.write("""\t\t\t\t<intensities unit="counts">""")
            for intensity in self.__intensities:
                if is_first:
                    is_first = False
                else:
                    out_file.write(" ")
                out_file.write("{0:.0f}".format(intensity))
            out_file.write("</intensities>\n")
            
            out_file.write(
"""			</dataPoints>
		</scan>
	</xrdMeasurement>
</xrdMeasurements>
""")
        
        print("File '{0}' written".format(output_file_name))
    
    def convert_file(self):
        self.__read_input_file()
        if len(self.__intensities) == 0:
            print("No intensities read from file '{0}'".format(
                    self.__file_name))
        else:
            self.__write_output_file()


def print_usage():
    print("Usage:")
    print("   python xy2xrdml.py -l <wavelength> -e <file extension>")
    print("Use --help to print all options")


def print_help():
    print("Options:")
    print("   -l or --lambda: set wavelength")
    print("   -e or --extension: set file extension to look for")
    print("   --help: print this message")


# -----------------------------------------------------------------------------
#   Main program
# -----------------------------------------------------------------------------

from pathlib import Path
import sys
import getopt


wavelength = None
file_extension = None

# If there are no args, print usage
if len(sys.argv) < 2:
    print_usage()
    sys.exit()

# Parse argv
try:
    opts, args = getopt.getopt(sys.argv[1:],
                               "l:e:",
                               ["lambda=", "extension=", "help"])
except getopt.GetoptError:
    print_usage()
    sys.exit()

# Extract arguments
for opt, arg in opts:
    if opt == "-l" or opt == "--lambda":
        try:
            wavelength = float(arg)
        except ValueError:
            print("Wavelength '{0}' could not be converted to a number"
                  .format(arg))
            sys.exit()
    elif opt == "-e" or opt == "--extension":
        file_extension = arg
    elif opt == "--help":
        print_help()
        sys.exit()

# Check if all required arguments are present
if wavelength == None or file_extension == None:
    print("Not all required arguments are present. Wavelength and "
          "file extension are needed.")
    sys.exit()
  
# Search for data files and convert them
working_dir = Path.cwd()
file_extension = file_extension.lower()
# Print arguments
print("Searching for files with extension '{0}' in directory '{1}'"
      .format(file_extension, working_dir))
print("Using wavelength {0} Å".format(wavelength))
print()

for file in working_dir.iterdir():
    if str(file).lower().endswith(file_extension):
        if file.is_file():
            converter = Xy2Xrdml(str(file), wavelength)
            converter.convert_file()
        else:
            print("'{0}' is not a file, skipping".format(str(file)))
